from contextlib import closing
from functools import partialmethod
from time import time
from typing import Any, Callable, Dict, Iterator, Mapping, Optional, Tuple, \
    Union

from openpyxl import Workbook, load_workbook
# noinspection PyProtectedMember
from openpyxl.worksheet._read_only import ReadOnlyWorksheet

from .base import BaseColumnFileReader

__all__ = [
    'ExcelReader',
]


class ExcelReader(BaseColumnFileReader):

    def _read_worksheet(
        self,
        worksheet: ReadOnlyWorksheet,
        columns_map: Optional[Mapping[str, str]],
        title_row_number: int,
        row_number_key: Optional[str],
    ) -> Iterator[Dict]:
        """Считывание листа книги."""
        logger = self._logger
        start_time = time()

        logger.info('Начинаем считывание листа {!r}'.format(worksheet.title))

        if worksheet.max_row < title_row_number:
            logger.warning('Лист пустой, считывание завершено.')
            return

        yield from self._iter_rows(
            worksheet.iter_rows(
                min_row=title_row_number,
                values_only=True
            ),
            columns_map=columns_map,
            start_row_number=title_row_number + 1,
            row_number_key=row_number_key,
        )

        logger.info('Считывание листа {!r} успешно завершено за {:.2f} секунд.'
                    .format(worksheet.title, time() - start_time))

    def _read_workbook(self,
                       handler: Callable,
                       filepath: str,
                       *args,
                       **kwargs) -> Any:
        """Считывание книги."""
        self._logger.info('Начато считывание книги {!r}.'.format(filepath))

        with closing(
            load_workbook(filepath, read_only=True, data_only=True)
        ) as workbook:
            return handler(self, workbook, *args, **kwargs)

    def _read_one(
        self,
        workbook: Workbook,
        worksheet_identity: Optional[Union[str, int]] = None,
        columns_map: Optional[Mapping[str, str]] = None,
        title_row_number: int = 1,
        row_number_key: Optional[str] = 'ROW_NUMBER',

    ) -> Tuple[Dict, ...]:
        """Считывание одного листа."""
        if isinstance(worksheet_identity, int):
            worksheet = workbook.worksheets[worksheet_identity]

        elif isinstance(worksheet_identity, str):
            worksheet = workbook[worksheet_identity]

        else:
            # Если ничего не указано - просто читаем первый лист.
            worksheet = workbook.worksheets[0]

        return tuple(self._read_worksheet(
            worksheet,
            columns_map,
            title_row_number,
            row_number_key,
        ))

    read_one = partialmethod(_read_workbook, _read_one)

    def _read_all(
        self,
        workbook: Workbook,
        columns_map: Optional[Mapping[str, str]] = None,
        title_row_number: int = 1,
        row_number_key: Optional[str] = 'ROW_NUMBER',
    ) -> Dict[Union[str, int], Tuple[Dict, ...]]:
        """Считывание всех листов."""
        results = {}
        for index, worksheet in enumerate(workbook.worksheets):
            data = tuple(self._read_worksheet(
                worksheet,
                columns_map,
                title_row_number,
                row_number_key,
            ))
            results[index] = results[worksheet.title] = data

        return results

    read_all = partialmethod(_read_workbook, _read_all)

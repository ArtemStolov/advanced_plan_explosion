from setuptools import find_packages, setup

setup(
    name='advanced_plan_explosion',
    version='0.3.0',
    packages=find_packages(),
    url='-',
    license='-',
    author='BFG-Soft LLC',
    author_email='info@bfg-soft.ru',
    description='Plan explosion advanced logic.',
    install_requires=[
        'PyYAML',
        'python-dateutil',
        'openpyxl>=2.6.0',
    ],
    extras_require={
        'dev': [
            'pytest',
        ]
    },
    entry_points={
        'console_scripts': [
            'advanced_plan_explosion = '
            'advanced_plan_explosion.script:main',
        ]
    }
)

import math
from collections import defaultdict
from datetime import datetime, timedelta
from time import time

from advanced_plan_explosion.base import Base
from advanced_plan_explosion.io import read_column_file
from advanced_plan_explosion.model import Factory, Plan

__all__ = [
    'CalcSession',
]


def _calculate_finish_date(start: datetime,
                           work_time_hours: float,
                           holidays: list,
                           day_off: list) -> datetime:
    work_hours = 0
    current_date = start

    while work_hours < work_time_hours:
        current_date += timedelta(1)
        if current_date.date() not in holidays \
                and current_date.isoweekday() not in day_off:
            work_hours += 24
    return current_date


def _calculate_start_date(finish: datetime,
                          work_time_hours: float,
                          holidays: list,
                          day_off: list) -> datetime:
    work_hours = 0
    current_date = finish

    while work_hours < work_time_hours:
        current_date -= timedelta(1)
        if current_date.date() not in holidays \
                and current_date.isoweekday() not in day_off:
            work_hours += 24
    return current_date


class CalcSession(Base):
    # сессия расчета объединяет план и завод для расчета загрузки

    def __init__(self,
                 entities=None,
                 plan=None,
                 factory=None,
                 *args,
                 **kwargs):
        super().__init__(*args, **kwargs)
        if plan is None:
            self.plan = Plan(entities, 'Новый план')
        else:
            self.plan = plan
        if factory is None:
            self.factory = Factory()
        else:
            self.factory = factory
        self.entities = self.plan.entities
        self.overload = dict()

    def calculate_workcenter_utilization(self, period_hours=1):
        report = {}
        for each_work_center in self.factory:
            report[each_work_center.id + '_' + each_work_center.name] = \
                each_work_center.workload_percents(self.plan, period_hours)
        return sorted(report.items(), key=lambda t: t[1], reverse=True)

    def big_workload_report(self, config):
        """
        Генерация отчета по загрузке всех рабочих центров по плану в разрезе
        всех изделий плана
        Изделия сортированы по загрузке узкого места, рабочие центры
        сортированы по загрузке
        :return:
        """
        start_date = datetime.strptime(config['start'], "%d/%m/%Y %H:%M")
        holidays = [
            datetime.strptime(
                x, '%Y/%m/%d'
            ).date() for x in config['holidays']
        ]
        day_off = config['day-off']
        accuracy = config['priority_accuracy']

        logger = self._logger
        logger.debug(
            '\n \n ### Ниже представлен отчет по загрузке '
            'самых загруженных классов РЦ ###'
        )
        i = 0
        for each_wc in self.factory.workcenter_sorted_list(self.plan):
            i += 1
            if i > 5:
                break
            last_date = _calculate_finish_date(
                start=start_date,
                holidays=holidays,
                day_off=day_off,
                work_time_hours=(
                        each_wc.workload(self.plan)
                        / each_wc.amount / each_wc.utilization_factor
                )
            ).date()
            logger.debug(
                'РЦ: {} - {}, Кол-во: {} шт., '
                'Загрузка средняя на 1 станок: {:.2f} ч.,'
                'Ожидаемая дата завершения работ {}'
                .format(
                    each_wc.id,
                    each_wc.name,
                    each_wc.amount,
                    each_wc.workload(self.plan) / each_wc.amount,
                    last_date
                )
            )

            for each_entry in self.plan.sorted_by_bottleneck_plan_entry_set(
                    self.factory,
                    accuracy=accuracy
            ):
                whole_workload = each_entry.product.whole_operation_workload[
                                     each_wc.id] * each_entry.amount + \
                                 each_entry.product.whole_preparing_workload[
                                     each_wc.id] * each_entry.number_of_batches
                if each_entry.amount \
                        * each_entry.product.whole_workload[each_wc.id] > 0:
                    logger.debug(
                        '    Заказ: {} Номенклатура: {} - {}, ' 
                        'Кол-во в плане: {}, '
                        'Загрузка от 1 штуки: {:.5f}, '
                        'Загрузка на весь план: {:.2f}'
                        .format(
                            each_entry.order_name,
                            each_entry.product.id,
                            each_entry.product.name,
                            each_entry.amount,
                            each_entry.product.whole_workload[each_wc.id],
                            whole_workload
                        )
                    )

    @property
    def calculate_workcenter_workload(self):
        report = {}
        for each_work_center in self.factory:
            report[each_work_center.id] = each_work_center.workload(self.plan)
        return report

    @property
    def product_entry_labor_report(self):
        report = {}
        for entry in self.plan:
            report[entry.product] = entry.labor
        return sorted(report.items(), key=lambda t: t[1], reverse=True)

    def get_workcenters_workload(self, date_from=datetime.min,
                                 date_to=datetime.max):
        report = defaultdict(float)
        for entry in reversed(self.plan.plan_entry):
            if not date_from < entry.date <= date_to:
                break
            for wc_id in entry.product.whole_workload:
                report[wc_id] += entry.product.whole_operation_workload[
                                     wc_id] * entry.amount
                report[wc_id] += entry.product.whole_preparing_workload[
                                     wc_id] * entry.number_of_batches

        return report

    def add_operation(self, product_id, wc_id, t_pz=0.0, t_sht=0.0):
        product = self.entities.get_product_with_id(product_id=product_id)
        wc = self.factory.get_workcenter_with_id(wc_id)

        if wc is not None:
            product.tech_proc.add_operation(wc, t_pz=t_pz, t_sht=t_sht)

    def read_tech_from_column_file(self, config):

        logger = self._logger
        data = read_column_file(config)

        unique_op_id = []

        progress = 0
        for row in data:

            if row['ROW_NUMBER'] / len(data) * 100 >= progress:
                logger.info(
                    'Обработано {}% строк файла технологий'
                    .format(progress)
                )
                progress += 10

            if row['ROW_NUMBER'] == 1:  # Пропускаем строку с заголовком
                continue

            if row['OP_ID'] in unique_op_id:
                logger.warning(
                    'Операция с ID {} встретилась повторно '
                    'и была проигнорирована'.format(row['OP_ID']))
                continue
            unique_op_id.append(row['OP_ID'])

            product_id = row['PRODUCT']
            try:
                if type(row['T_SHT']) is str:
                    tsht = float(row['T_SHT'].replace(',', '.'))
                else:
                    tsht = float(row['T_SHT'])
            except (ValueError, TypeError):
                tsht = 0
                if row['T_SHT'] is not None:
                    logger.warning(
                        'Неверное значение ячейки столбца T_SHT в строке {}, '
                        'значение в ячейке {}'.format(row['ROW_NUMBER'],
                                                      row['T_SHT']))
            try:
                if type(row['T_PZ']) is str:
                    tpz = float(row['T_PZ'].replace(',', '.'))
                else:
                    tpz = float(row['T_PZ'])
            except (ValueError, TypeError):
                tpz = 0
                if row['T_PZ'] is not None:
                    logger.warning(
                        'Неверное значение ячейки столбца T_PZ в строке {}, '
                        'значение в ячейке {}'.format(row['ROW_NUMBER'],
                                                      row['T_PZ']))
            wc_id = str(row['EQUIPMENT']) + '_' + str(row['DEPARTMENT'])
            product = self.entities.get_product_with_id(product_id)
            if product is not None:
                self.add_operation(product_id, wc_id, t_pz=tpz / 60,
                                   t_sht=tsht / 60)

    def is_moveto_allowed(self, entry, time_hours, from_plan,
                          max_product_share,
                          current_date, wc_share=False,
                          factory_workload=None, basic_plan=None):
        """
        проверяем можно ли изделие из указанной строки плана перекинуть
        в целевой план
        загрузку рабочих центров проверяем по периоду от текущей даты
        на time_hours часов назад
        :param entry: строка плана, изделие из которой (в размере батча
        или остатка) хотим перекинут в новый план
        :param from_plan: из этого плана забираем это изделие
        :param time_hours: шаг плана
        :param max_product_share: ограничение, что на каждом ресурсе доля
        одного изделия должна быть не более этого значения
        :param current_date: дата, на которую проверяем возможность
        добавления изделий в план
        :param wc_share: автоматически расчитанное ограничение на загрузку
        станков одной номенклатурой
        :param basic_plan: исходный план, чтоб посчитать wc_share
        :param factory_workload: загрузка всех станков, чтоб каждый раз
        не пересчитывать
        :return: на выходе True, если изделие можно добавить в план и False,
        если нельзя
        """
        logger = self._logger
        date_from = current_date - timedelta(time_hours / 24)
        date_to = current_date

        if basic_plan is None:
            basic_plan = from_plan

        # если изделия уже нет в плане, то его и нельзя переместить
        if entry not in from_plan:
            logger.debug(
                'Изделие {} в плане закончилось'.format(entry.product.name)
            )
            return False
        if factory_workload is None:
            factory_workload = self.get_workcenters_workload(
                date_from=date_from, date_to=date_to)

        # проверяем по каждому станку соблюдение всех условий
        # загрузки сортируем от самого загруженного
        for workcenter in self.factory.workcenter_sorted_list(basic_plan):

            wc_workload = factory_workload[workcenter.id]
            batch = min(entry.product.batch_size, entry.amount)

            #  Фонд допустимой загрузки станка
            wc_time_fund = \
                time_hours * workcenter.utilization_factor * workcenter.amount
            #  Вычтем из фонда доступного времени перегрузку прошлого дня
            wc_time_fund_without_overhead = \
                wc_time_fund - self.overload[workcenter.id][-1]

            if (wc_workload > wc_time_fund_without_overhead) and \
                    (entry.product.whole_workload[workcenter.id] > 0):
                # даже если станок перегружен, но изделие не занимает
                # этот станок совсем, то его (изделие) можно в план включить
                logger.debug(
                    'Изделие {} из заказа {} не будет '
                    'добавлено в план потому, что станок '
                    '{} уже перегружен'.format(
                        entry.product.code,
                        entry.order_name,
                        workcenter.name
                    )
                )
                return False

            if wc_share:
                try:
                    # Определяем сколько станков можем занять одной
                    # номенклатурой
                    tmp_ = workcenter.amount * (
                            entry.product.whole_workload[workcenter.id] *
                            basic_plan.amount_of_products(entry.product) /
                            workcenter.workload(basic_plan))
                    wc_amount = math.ceil(
                        tmp_)  # округление до ближайшего большего числа
                    if wc_amount == 0:
                        wc_amount = workcenter.amount
                except ZeroDivisionError:
                    wc_amount = workcenter.amount
                # сколько добавляемое изделие уже загружает этот станок за
                # рассматриваемый период сравниваем с предельной загрузкой
                # расчетного количества станков
                if entry.product.whole_workload[workcenter.id] * \
                        (self.plan.amount_of_products(
                            entry.product,
                            date_from=date_from,
                            date_to=date_to
                        ) + batch) \
                        > time_hours * workcenter.utilization_factor \
                        * wc_amount:
                    logger.debug(
                        'Изделие {} не будет добавлено в план потому, '
                        'что ему нельзя загружать более {} станков в '
                        'классе {}'
                        .format(entry.product.code, wc_amount, workcenter.name)
                    )
                    return False

            # сколько добавляемое изделие уже загружает этот станок за
            # рассматриваемый период сравниваем с предельной загрузкой с
            # учетом максимальной загрузки станка одной номенклатурой
            if entry.product.whole_workload[workcenter.id] * \
                    self.plan.amount_of_products(
                        entry.product,
                        date_from=date_from,
                        date_to=date_to
                    ) \
                    > time_hours * workcenter.utilization_factor * \
                    max_product_share * workcenter.amount:
                # проверяем соответствует ли условию по ограничению
                # занятия ресурса одной номенклатурой
                logger.debug(
                    'Изделие {} не будет добавлено в план потому, что ему '
                    'нельзя загружать рабочий центр {} с долей больше {}'
                    .format(
                        entry.product.code,
                        workcenter.name,
                        max_product_share
                    )
                )
                return False

        return True

    def get_exploded_session(self, rules, from_date=datetime.min,
                             to_date=datetime.max):
        """
        метод проведения разбиения плана по различным логикам
        :param from_date начала диапазона, из которого можно брать заказы в
        распределение
        :param to_date конец диапазона, из которого можно брать заказы в
        распределение
        :param rules: словарь с правилами планирования
        ['step'] шаг планирования
        ['recalculate_priority']: пересчитываем ли приоритеты строк плана
        на каждом шаге
        ['max_product_share']: ограничение на занятие ресурса одной
        номенклатурой
        ['with_overhead']: Планируем 'с перебором' или с недобором
        ['deep_steps']: на какую глубину плана анализируем загрузку
        рабочих центров
        ['start_date']: стартовая дата начала выполнения плана
        ['simplify_plan']: схлопывать ли план до деталей перед расчетом
        :return: на выходе расчетная сессия и количество дней,
            которое понадобилось чтоб запустить все изделия в производство
        """
        logger = self._logger
        logger.info('Начинаем формирование плана запуска')

        # если задано from_date, то с него начинаем распределение,
        # 'start' игнорируем
        if from_date == datetime.min:
            start_date = datetime.strptime(
                rules['start'],
                '%d/%m/%Y %H:%M'
            )
        else:
            start_date = from_date

        step_duration_hours = rules['step']
        max_product_share = rules['max_product_share']
        wc_share = rules['wc_share']
        manual_priority = rules['manual_priority']
        simplify_plan = rules['simplify_plan']
        collapse_plan = rules['collapse_plan']
        holidays = [
            datetime.strptime(
                x, '%Y/%m/%d'
            ).date() for x in rules['holidays']
        ]

        work_plan = self.plan.copy(from_date, to_date)
        basic_plan = self.plan.copy(from_date, to_date)

        if simplify_plan:
            work_plan = work_plan.simple_plan()

        # создаем временную копию плана, чтоб с ней проводить манипуляции,
        # а основной план оставить в исходном виде
        result_session = CalcSession(
            factory=self.factory,
            plan=Plan(name='план после explosion',
                      entities=self.entities)
        )

        # запускаем счетчик часов и шагов
        hour = 0

        if collapse_plan:
            work_plan.collapse()
        # схлопываем план перед разбиением (все одинаковые изделия
        # сводим в одну строку)

        # сортируем план по приоритету (в данном случае по загрузке УМ)
        if not manual_priority:
            sorted_plan = work_plan.sorted_by_bottleneck_plan_entry_set(
                self.factory, accuracy=rules['priority_accuracy'])
        else:
            sorted_plan = work_plan.plan_entry
        total_plan_amount = work_plan.total_amount

        for workcenter in result_session.factory.workcenter:
            result_session.overload.update({workcenter.id: [0]})

        while work_plan.plan_entry:
            t = time()
            current_date = start_date + timedelta(hour / 24)
            while current_date.date() in holidays \
                    or current_date.isoweekday() in rules['day-off']:
                hour += 1
                current_date += timedelta(1 / 24)
            date_from = current_date - timedelta(step_duration_hours / 24)
            date_to = current_date
            factory_workload = result_session.get_workcenters_workload(
                date_from=date_from, date_to=date_to)
            # здесь надо посчитать перегрузку с прошлого периода,
            # которую надо учесть в этом периоде
            # перебираем строки плана в исходной сессии в порядке приоритета
            not_allowed_list = []
            for entry in sorted_plan:
                if entry.product in not_allowed_list:
                    continue
                entry_start_date = _calculate_start_date(
                    finish=entry.date,
                    work_time_hours=rules['max_advance_period'] * 24,
                    holidays=holidays,
                    day_off=rules['day-off']
                )
                if entry_start_date > current_date:
                    continue
                if entry.amount == 0:
                    continue
                # проверяем можно ли еще один батч закинуть в план
                number_of_batches = entry.min_number_of_batches
                while result_session.is_moveto_allowed(
                        entry=entry,
                        from_plan=work_plan,
                        time_hours=step_duration_hours,
                        max_product_share=max_product_share,
                        current_date=current_date,
                        wc_share=wc_share,
                        factory_workload=factory_workload,
                        basic_plan=basic_plan,
                ):
                    # если влазит еще один батч (по загрузке оборудования),
                    # то добавляем
                    work_plan.moveto(
                        entry=entry,
                        other_plan=result_session.plan,
                        date=(start_date + timedelta(hour / 24)),
                        number_of_batches=number_of_batches
                    )
                    number_of_batches = 1
                    # и пересчитываем загрузку
                    factory_workload = \
                        result_session.get_workcenters_workload(
                            date_from=date_from,
                            date_to=date_to
                        )
                if entry in work_plan:
                    not_allowed_list.append(entry.product)
            not_allowed_list = []
            if max_product_share != 1 or wc_share:
                for entry in sorted_plan:
                    if entry.product in not_allowed_list:
                        continue

                    entry_start_date = _calculate_start_date(
                        finish=entry.date,
                        work_time_hours=rules['max_advance_period'] * 24,
                        holidays=holidays,
                        day_off=rules['day-off']
                    )

                    if entry_start_date > current_date:
                        continue

                    if entry.amount == 0:
                        continue
                    # проверяем можно ли еще один батч закинуть в план
                    while result_session.is_moveto_allowed(
                            entry=entry,
                            from_plan=work_plan,
                            time_hours=step_duration_hours,
                            max_product_share=1,
                            current_date=current_date,
                            wc_share=False,
                            factory_workload=factory_workload,
                            basic_plan=basic_plan,
                    ):
                        # если влазит еще один батч (по загрузке оборудования),
                        # то добавляем
                        work_plan.moveto(entry=entry,
                                         other_plan=result_session.plan,
                                         date=(start_date + timedelta(
                                             hour / 24)))
                        # и пересчитываем загрузку
                        factory_workload = \
                            result_session.get_workcenters_workload(
                                date_from=date_from,
                                date_to=date_to
                            )
                    if entry in work_plan:
                        not_allowed_list.append(entry.product)

            work_hours = 0

            while work_hours < step_duration_hours:
                hour += 1  # счетчик часов
                calc_date = start_date + timedelta(hour / 24)
                if calc_date.isoweekday() not in rules['day-off'] \
                        and calc_date.date() not in holidays:
                    work_hours += 1
            for workcenter in result_session.factory.workcenter:
                wc_id = workcenter.id
                #  считаем перегрузку текущего шага
                #  (если отрицательное число, то недогрузка)
                overload = factory_workload[wc_id]
                overload -= \
                    step_duration_hours * workcenter.utilization_factor * \
                    workcenter.amount

                #  если текущая перегрузка плюс вчерашняя больше нуля,
                #  то сохраняем ее и учтем на следующий шаг
                if overload + result_session.overload[wc_id][-1] > 0:
                    result_session.overload[wc_id].append(
                        overload + result_session.overload[wc_id][-1])
                #  если меньше нуля, то недогрузка на следующий день
                #  не переносится
                else:
                    result_session.overload[wc_id].append(0)

            logger.info(
                '{} часов {}'
                .format(hour, current_date)
            )

            logger.info(
                'Прогресс: {:.2%}'
                .format(result_session.plan.total_amount / total_plan_amount)
            )

            logger.info(
                'Формирование плана на один шаг заняло {:.2f} секунд'
                .format(time() - t)
            )

        logger.info(
            'План запуска сформирован, все изделия плана '
            'будут запущены в производство за {} часов'
            .format(str(hour))
        )

        report = result_session.calculate_workcenter_utilization(
            period_hours=hour
        )

        for row in report:
            if row[1] > 0:
                logger.info(row)

        return result_session

    def get_exploded_session_with_separated_months(self, rules):
        logger = self._logger
        result_session = CalcSession(entities=self.entities,
                                     factory=self.factory)

        from_date = self.plan.start_date

        while from_date <= self.plan.finish_date:
            to_date = datetime(from_date.year + from_date.month // 12,
                               ((from_date.month % 12) + 1), 1)
            logger.info(from_date, to_date)
            exploded_session = self.get_exploded_session(rules,
                                                         from_date=from_date,
                                                         to_date=to_date)
            result_session.plan.concatenate_with(exploded_session.plan)
            from_date = datetime(from_date.year + int(from_date.month / 12),
                                 ((from_date.month % 12) + 1), 1)

        return result_session
    
    def read_from_column_file(self, config):
        # читаем все данные
        self.entities.read_spec_from_column_file(
            config['spec'],
        )
        self.factory.read_from_column_file(
            config['equipment'],
        )
        self.read_tech_from_column_file(
            config['tech'],
        )
        self.entities.read_batch_size_from_column_file(
            config['batch_size'],
        )
        self.plan.read_plan_from_column_file(
            config['plan'],
            config['rules']['default_batch_size'],
            config['rules'].get('default_number_of_batches', 1),
        )

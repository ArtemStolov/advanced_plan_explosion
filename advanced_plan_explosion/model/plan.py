import math
from collections import defaultdict
from datetime import datetime, timedelta
from typing import Dict, Iterable, Iterator, Union

from advanced_plan_explosion.base import Base
from advanced_plan_explosion.io import read_column_file
from .factory import Factory

__all__ = [
    'PlanEntry',
    'Plan',
]


class PlanEntry(object):  # строка плана
    def __init__(self, product, amount, order_name, initial_name=None,
                 initial_order=None, initial_code=None,
                 entry_date=datetime.now(),
                 min_number_of_batches=1):
        self.product = product
        self.amount = amount
        self.date = entry_date
        self.order_name = order_name
        self.initial_name = initial_name
        self.initial_code = initial_code
        self.initial_order = initial_order
        self.min_number_of_batches = min_number_of_batches
        self.number_of_batches = math.ceil(
            round(self.amount, 2) / round(self.product.batch_size, 2))

    def __repr__(self):
        return self.product.description + 'Количество:' + str(
            self.amount) + ' Дата:' + str(self.date) + '\n'

    @property
    def description(self):
        return self.product.description + 'Количество:' + str(
            self.amount) + ' \nДата:' + str(self.date) + '\n \n'

    @property
    def labor(self):
        return self.product.labor_per_part * self.amount

    def workload(self,
                 work_center):
        # загрузка выбранного оборудования по строке плана
        workload = self.product.whole_operation_workload[
                       work_center.id] * self.amount
        workload += self.product.whole_preparing_workload[
                        work_center.id] * self.number_of_batches
        return workload


class Plan(Base, Iterable):  # план

    def __init__(self, entities, name='', *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name
        self.plan_entry = []
        self.entities = entities

    def __repr__(self):
        report = 'План:' + self.name + '\n'
        for entry in self.plan_entry:
            report += entry.description
        return report

    def __iter__(self) -> Iterator[PlanEntry]:
        return iter(self.plan_entry)

    def concatenate_with(self, other):
        for entry in other.plan_entry:
            self.plan_entry.append(entry)

    @property
    def start_date(self):
        result = datetime.max
        for entry in self.plan_entry:
            result = min(result, entry.date)
        return result

    @property
    def finish_date(self):
        result = datetime.min
        for entry in self.plan_entry:
            result = max(result, entry.date)
        return result

    @property
    def number_of_entries(self):  # количество строк в плане
        return len(self.plan_entry)

    @property
    def total_amount(self):  # количество штук/метров в плане
        total_amount = 0
        for entry in self.plan_entry:
            total_amount += entry.amount
        return total_amount

    @property
    def product_set(self):  # множество уникальных продуктов в плане
        # product_set = set()
        # for entry in self.plan_entry:
        #     product_set.add(entry.product)
        # return product_set
        return set(self.plan_entry)

    @property
    def sorted_product_set(
            self):
        # множество уникальных продуктов в плане, сортированных по трудоемкости
        return sorted(self.product_set,
                      key=lambda product: product.labor_per_plan(self),
                      reverse=True)

    @property
    def sorted_plan_entry_set(self):
        # строки плана, сортированные по трудомекости
        return sorted(self.plan_entry, key=lambda entry: entry.labor,
                      reverse=True)

    def exclude_dayoffs(self, weekdays, holidays_dates):
        add_days = 0
        for entry in sorted(self.plan_entry, key=lambda x: x.date):
            entry.date += timedelta(days=add_days)
            while (entry.date.isoweekday() in weekdays) or (
                    entry.date in holidays_dates):
                add_days += 1
                entry.date += timedelta(days=1)

    def simple_plan(self):
        """
        На входе план со сложными изделиями, на выходе план только
        с деталями в количестве, необходимом для производства всех
        изделий плана
        :return:
        """
        result_plan = Plan(self.entities)
        for entry in self.plan_entry:
            for part in entry.product.simplify_specification_dict(
                    entry.amount):
                amount = \
                    entry.product.simplify_specification_dict(entry.amount)[
                        part]
                part_id = part.id
                result_plan.add(part_id, amount, entry.date)
        return result_plan

    def sorted_by_bottleneck_plan_entry_set(self, factory=Factory(),
                                            accuracy=1):
        # строки плана, сортированные по загрузке самых загруженных классов РЦ
        wc_sorted_list = factory.workcenter_sorted_list(self)
        result = self.plan_entry
        # сортируем последовательно от незагруженных
        # к самым загруженным рабочим центрам
        for i in range(len(wc_sorted_list) - 1, -1, -1):
            result = sorted(result, key=lambda entry: round(
                entry.workload(wc_sorted_list[i]) / accuracy / entry.amount),
                            reverse=True)
        result = sorted(result, key=lambda entry: entry.date)

        return result

    def add(self, product_id, amount=0.0, date=datetime.now(),
            code=None, name=None, order_name=None,
            min_number_of_batches=1):  # добавить строку в план
        product = self.entities.get_product_with_id(product_id)
        logger = self._logger
        if amount == 0:
            logger.warning(
                'Нулевое количество по продукту {}'
                .format(product_id)
            )
            return

        if product is None:

            if code is None and name is None:
                logger.warning('Поля IDENTITY и NAME отсутствуют '
                               'для продукта {}.'.format(product_id))
                return

            logger.warning(
                'Не найден продукт {}, пробуем определять '
                'по поляем IDENTITY и NAME'.format(product_id)
            )

            product = self.entities.get_product_with_code_name(code, name)
            if product is None:
                logger.warning(
                    'Не найден продукт {} {}, пробуем определить '
                    'только по IDENTITY'.format(code, name)
                )

                if code is None:
                    logger.warning('Поле IDENTITY отсутствует '
                                   'для продукта {}.'.format(product_id))
                    return

                product = self.entities.get_product_with_code(code)
                if product is None:
                    logger.warning('Не найден продукт {}'.format(code))
                    return
                else:
                    logger.warning('Найден продукт {}'.format(product.id))

            else:
                logger.warning('Найден продукт {}'.format(product.id))

        self.plan_entry.append(
            PlanEntry(product=product, amount=amount, entry_date=date,
                      order_name=order_name,
                      initial_name=name, initial_code=code,
                      initial_order=order_name,
                      min_number_of_batches=min_number_of_batches))

    def clean(self):  # удалить все строки из плана
        self.plan_entry.clear()

    def remove(self, entry):
        self.plan_entry.remove(entry)

    def copy(self, from_date, to_date):
        """
        Копирует строки плана в другой план, с ограничением
        по дате запуска строк плана
        :param from_date: от этой даты
        :type from_date: datetime
        :param to_date: и до этой даты
        :type to_date: datetime
        :return: возвращает план
        """
        temp_copy = Plan(self.entities)
        for each in self.plan_entry:
            if from_date <= each.date < to_date:
                temp_copy.add(product_id=each.product.id, amount=each.amount,
                              date=each.date, order_name=each.order_name,
                              code=each.initial_code, name=each.initial_name,
                              min_number_of_batches=each.min_number_of_batches)
        return temp_copy

    def workload(self, work_center):
        workload = 0
        for entry in self:
            workload += entry.workload(work_center)
        return workload

    def amount_of_products(self, product, date_from=datetime.min,
                           date_to=datetime.max):
        count = 0
        for entry in self:
            if (entry.product == product) and (
                    date_from < entry.date <= date_to):
                count += entry.amount
        return count

    def labor_per_product(self, product):
        labor = 0
        for entry in self:
            if entry.product == product:
                labor += entry.product.labor_per_part * entry.amount
        return labor

    def get_tech_proc_of_product_with_id(self, product_id):
        return self.get_product_with_id(product_id).tech_proc

    def get_product_with_id(self, product_id):
        for entry in self:
            if entry.product.id == product_id:
                return entry.product
        return None

    def read_plan_from_column_file(self,
                                   config,
                                   default_batch_operation_hours,
                                   default_number_of_batches):
        # считываем из xlsx файла план

        logger = self._logger
        data = read_column_file(config)

        for row in data:
            if row['ROW_NUMBER'] == 1:
                continue
            order_name = row['ORDER']
            product_id = row['CODE']
            product_name = row.get('NAME')
            product_code = row.get('IDENTITY')

            try:
                if type(row['AMOUNT']) is str:
                    entry_amount = float(row['AMOUNT'].replace(',', '.'))
                else:
                    entry_amount = float(row['AMOUNT'])
            except(ValueError, AttributeError):
                logger.warning(
                    'Неверное значение ячейки столбца '
                    'AMOUNT в строке {}, значение ячейки {}'
                    .format(row['ROW_NUMBER'], row['AMOUNT'])
                )
                continue

            if 'NUMBER_OF_BATCHES' in row:
                number_of_batches = row['NUMBER_OF_BATCHES']
                try:
                    min_number_of_batches = int(number_of_batches)

                except(ValueError, TypeError):
                    logger.warning(
                        'Неверное значение ячейки столбца '
                        'NUMBER_OF_BATCHES в строке {}, значение ячейки {}.'
                        .format(row['ROW_NUMBER'], number_of_batches)
                    )
                    continue
            else:
                min_number_of_batches = default_number_of_batches

            try:
                entry_date = datetime.combine(datetime.date(row['DATE']),
                                              datetime.time(row['DATE']))
            except(TypeError, ValueError, AttributeError):
                entry_date = datetime.now()
                logger.warning(
                    'Неверное значение ячейки столбца '
                    'DATE в строке {}, значение ячейки {}'
                    .format(row['ROW_NUMBER'], row['DATE'])
                )
            if entry_amount > 0:
                product = self.entities.get_product_with_id(product_id)
                if product is not None:
                    if product.batch_size == 0:
                        product.calculate_batch_size(
                            default_batch_operation_hours
                        )
                self.add(product_id=product_id, amount=entry_amount,
                         date=entry_date,
                         code=product_code, name=product_name,
                         order_name=order_name,
                         min_number_of_batches=min_number_of_batches)

            else:
                logger.warning(
                    'Изделие {} из заказа {} в плане с '
                    'нулевым количеством. При расчете учтено не будет'
                    .format(product_id, order_name)
                )

        self.entities.recalculate_entities_workload()

    def collapse(self):
        """
        метод для 'сворачиания' плана -- в нем остаются только
        уникальные изделия с суммами по количеству и самой ранней
        датой из всех строк
        """
        logger = self._logger
        product_basket = defaultdict(
            lambda: {'Amount': 0.0, 'Date': datetime.max})
        for entry in self:
            product_basket[entry.product]['Amount'] += entry.amount
            product_basket[entry.product]['Date'] = min(entry.date,
                                                        product_basket[
                                                            entry.product][
                                                            'Date'])

        self.clean()  # почистим все entry в заказе
        for entry in product_basket.keys():  # и заполним заново
            entry_amount = product_basket[entry]['Amount']
            entry_date = product_basket[entry]['Date']
            self.add(entry.id, entry_amount, entry_date)

        logger.info(
            'Сворачивание плана проведено успешно'
        )

    def moveto(self, other_plan, entry, date=datetime.now(),
               number_of_batches=1):

        logger = self._logger
        num = 1

        while num <= number_of_batches:

            other_plan.add(product_id=entry.product.id,
                           amount=min(round(entry.product.batch_size, 2),
                                      round(entry.amount, 2)),
                           date=date, order_name=entry.order_name,
                           code=entry.initial_code, name=entry.initial_name)
            logger.debug(
                'Добавлено изделие {} из заказа {}, размер партии {}'
                .format(
                    entry.product.code, entry.order_name,
                    min(entry.product.batch_size,
                        round(entry.amount, 2))
                )
            )
            if entry.amount > round(entry.product.batch_size, 2):
                entry.amount -= round(entry.product.batch_size, 2)
            else:
                entry.amount = 0
                self.remove(entry)
                break

            num += 1

        if (entry.amount >= number_of_batches * entry.product.batch_size) \
                or entry.amount == 0:
            return

        while entry:

            other_plan.add(product_id=entry.product.id,
                           amount=min(round(entry.product.batch_size, 2),
                                      round(entry.amount, 2)),
                           date=date, order_name=entry.order_name,
                           code=entry.initial_code, name=entry.initial_name)
            logger.debug(
                'Добавлено изделие {} из заказа {}, размер партии {}'
                .format(
                    entry.product.code,
                    entry.order_name,
                    min(entry.product.batch_size,
                        round(entry.amount, 2))
                )
            )
            if entry.amount > round(entry.product.batch_size, 2):
                entry.amount -= round(entry.product.batch_size, 2)
            else:
                entry.amount = 0
                self.remove(entry)
                break

    @property
    def column_file_data(self) -> Iterator[Dict[str, Union[str, int, float]]]:
        for index, entry in enumerate(self, start=1):
            yield {
                'INITIAL_ORDER': str(entry.initial_order),
                'INITIAL_IDENTITY': str(entry.initial_code or ''),
                'INITIAL_NAME': str(entry.initial_name or ''),
                'AMOUNT': entry.amount,
                'DATE': entry.date,
                'ORDER': '{}_{}_{}'.format(
                    entry.order_name,
                    index,
                    entry.product.code,
                ),
                'IDENTITY': entry.product.code,
                'NAME': entry.product.name,
                'CODE': entry.product.id
            }

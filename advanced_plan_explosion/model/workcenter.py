__all__ = [
    'WorkCenter',
]


class WorkCenter(object):  # рабочий центр
    def __init__(self, wc_id, name='', amount=1, utilization_factor=1):
        self.id = wc_id
        self.name = name
        self.amount = amount
        self.utilization_factor = utilization_factor

    def workload(self, plan, date_from=None, date_to=None):
        # загрузка рабочего центра по заданному плану
        workload = 0
        for entry in plan:
            if date_from is None and date_to is None:
                workload += entry.workload(self)
                continue
            if date_from < entry.date <= date_to:
                workload += entry.workload(self)
        return workload

    def workload_percents(self, plan,
                          period_hours):
        # загрузка выбранного рабочего центра в %
        if period_hours == 0:
            period_hours = 1
        return round(self.workload(plan) * 100 / self.amount / period_hours, 1)


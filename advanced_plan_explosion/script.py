from argparse import ArgumentParser
from logging import DEBUG, INFO, basicConfig
from os import getcwd
from os.path import join

from advanced_plan_explosion.config import read_config
from advanced_plan_explosion.logic import calculate

__all__ = [
    'main',
]


def main():
    parser = ArgumentParser(
        description='Инструмент для генерации плана запуска, обеспеченного '
                    'комплектующими для последующего его импорта в систему IA.'
    )
    parser.add_argument('-c', '--config', required=False,
                        default=join(getcwd(), 'plan_explosion.yml'))
    parser.add_argument('-d', '--debug', required=False, action='store_true',
                        default=False)

    args = parser.parse_args()

    basicConfig(level=args.debug and DEBUG or INFO)

    config = read_config(args.config)
    calculate(config)


if __name__ == '__main__':
    main()

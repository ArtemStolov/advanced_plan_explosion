from os.path import abspath, join

from pkg_resources import resource_filename
from pytest import fixture

__all__ = [
    'get_mock_filepath',
]


@fixture()
def get_mock_filepath():
    def get(*args):
        return abspath(resource_filename(
            'advanced_plan_explosion',
            join('tests', '_mock', *args)
        ))
    return get

from advanced_plan_explosion.io import CsvReader, ExcelReader


def test_excel_reader(get_mock_filepath):
    filepath = get_mock_filepath('test.xlsx')

    reader = ExcelReader()

    data = reader.read_one(filepath)
    assert data == ()

    data = reader.read_one(filepath, title_row_number=2)
    assert data == (
        {'A': 1, 'B': 'a', 'C': '/', 'ROW_NUMBER': 3},
        {'A': 7, 'B': 8, 'C': None, 'ROW_NUMBER': 5},
        {'A': '-', 'B': None, 'C': None, 'ROW_NUMBER': 9},
    )

    data = reader.read_one(filepath,
                           columns_map={'NOT_A': 'A', 'NOT_B': 'B'},
                           row_number_key=None,
                           title_row_number=2)
    assert data == (
        {'C': '/', 'NOT_A': 1, 'NOT_B': 'a'},
        {'C': None, 'NOT_A': 7, 'NOT_B': 8},
        {'C': None, 'NOT_A': '-', 'NOT_B': None},
    )

    all_data = reader.read_all(filepath,
                               columns_map={'NOT_A': 'A', 'NOT_B': 'B'},
                               row_number_key=None,
                               title_row_number=2)
    assert all_data == {
        0: data,
        'Лист1': data,
    }


def test_csv_reader(get_mock_filepath):
    filepath = get_mock_filepath('test.csv')

    reader = CsvReader()
    data = reader.read(filepath)

    assert data == (
        {'A': '1', 'B': 'a', 'C': '/', 'ROW_NUMBER': 2},
        {'A': '7', 'B': '8', 'C': '', 'ROW_NUMBER': 4},
        {'A': '-', 'B': '', 'C': '', 'ROW_NUMBER': 8}
    )

    data = reader.read(filepath,
                       columns_map={'NOT_A': 'A', 'NOT_B': 'B'},
                       row_number_key=None)

    assert data == (
        {'C': '/', 'NOT_A': '1', 'NOT_B': 'a'},
        {'C': '', 'NOT_A': '7', 'NOT_B': '8'},
        {'C': '', 'NOT_A': '-', 'NOT_B': ''}
    )

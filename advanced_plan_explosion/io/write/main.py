from typing import Iterable, Mapping

from .csv import write_csv_file
from .excel import write_excel_file
from ..abc import AbstractColumnFileIOHandler

__all__ = [
    'ColumnFileWriter',
    'write_column_file',
]


class ColumnFileWriter(AbstractColumnFileIOHandler):

    # noinspection PyMethodOverriding
    def _handle_excel(self,
                      filepath: str,
                      data: Iterable[Mapping],
                      worksheet_name: str,
                      *args,
                      **kwargs) -> None:
        return write_excel_file(
            filepath,
            worksheet_name,
            data,
            **kwargs,
        )

    # noinspection PyMethodOverriding
    def _handle_csv(self,
                    filepath: str,
                    data: Iterable[Mapping],
                    *args,
                    **kwargs) -> None:
        return write_csv_file(
            filepath,
            data,
        )

    def write(self, data: Iterable[Mapping], config: dict) -> None:
        return self._handle(
            config['file'],
            data,
            config.get('worksheet', 'Лист1'),  # имеет эффект только для excel.
            overwrite=config.get('overwrite', True),
            title=config.get('title'),
            format_columns=config.get('columns', True),
            format_cells=config.get('cells', True),
            format_page=config.get('page', True)
        )


write_column_file = ColumnFileWriter().write

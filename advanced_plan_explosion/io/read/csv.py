from csv import Sniffer, reader
from time import time
from typing import Dict, Mapping, Optional, Tuple

from .base import BaseColumnFileReader

__all__ = [
    'CsvReader',
]


_SNIFFER = Sniffer()


class CsvReader(BaseColumnFileReader):

    def read(self,
             filepath: str,
             columns_map: Optional[Mapping[str, str]] = None,
             row_number_key: Optional[str] = 'ROW_NUMBER') -> Tuple[Dict, ...]:
        logger = self._logger
        start_time = time()
        logger.info('Начинаем считывание csv файла {!r}.'.format(filepath))

        with open(filepath, 'r', newline='', encoding='utf-8') as file:
            data = ''.join(file.readlines(1024))

            if not data:
                logger.warning('Файл {!r} пуст.'.format(filepath))
                return ()

            dialect = _SNIFFER.sniff(
                data,
                delimiters=',;'
            )
            file.seek(0)

            rows = tuple(self._iter_rows(
                reader(file, dialect=dialect),
                columns_map=columns_map,
                row_number_key=row_number_key,
            ))

        logger.info('Считывание из файла {!r} успешно завершено за {:.2f} '
                    'секунд.'.format(filepath, time() - start_time))

        return rows

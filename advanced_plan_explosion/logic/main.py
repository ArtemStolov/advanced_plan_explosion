from datetime import datetime, timedelta
from typing import Any, Dict, Iterator, List

from advanced_plan_explosion.io import write_column_file
from advanced_plan_explosion.model import Entities, Plan, WorkCenter
from .calc import CalcSession

__all__ = [
    'calculate',
]


def _make_workload_report(rules: Dict[str, Any],
                          workcenters: List[WorkCenter],
                          plan: Plan) -> Iterator[Dict[str, Any]]:
    start_date = datetime.strptime(
        rules['start'],
        "%d/%m/%Y %H:%M"
    )
    for workcenter in workcenters:
        row = {
            'WORKCENTER': '{} - {} ({} шт, КИ {})'.format(
                workcenter.id,
                workcenter.name,
                str(workcenter.amount),
                str(round(workcenter.utilization_factor, 2))
            ),
            'NAME': workcenter.name,
            'AMOUNT': workcenter.amount,
            'UTILIZATION': round(workcenter.utilization_factor, 2)
        }

        finish_date = plan.finish_date
        day = timedelta(days=1)
        period_to = start_date
        period_from = period_to - day

        while period_to <= finish_date:
            wc_workload = workcenter.workload(
                plan,
                date_from=period_from,
                date_to=period_to,
            ) / 24 / workcenter.amount

            row[period_to.date()] = round(wc_workload * 100, ndigits=2)

            period_from = period_to
            period_to += day

        yield row


def calculate(config: dict):

    rules = config['rules']

    session = CalcSession(entities=Entities())
    session.read_from_column_file(config)
    session.big_workload_report(rules)

    if rules['separate_months']:
        result_session = session.get_exploded_session_with_separated_months(
            rules
        )
    else:
        result_session = session.get_exploded_session(rules)

    output_config = config['output']

    plan_config = output_config['plan']

    write_column_file(
        result_session.plan.column_file_data,
        plan_config,
    )

    workload_config = output_config.get('workload')
    if not workload_config:
        return

    if workload_config['file'] == plan_config['file']:
        workload_config['overwrite'] = False

    write_column_file(
        _make_workload_report(
            rules,
            result_session.factory.workcenter_sorted_list(
                result_session.plan
            ),
            result_session.plan,
        ),
        workload_config,
    )
